@extends('layout.master')

@section('judul')

Halaman Detail Kategori {{$genre->id}} 
    
@endsection


@section('content')
<h3> {{$genre->nama}} </h3>
<p> {{$genre->deskripsi}} </p>

<div class="row">
    @foreach ($genre->film as $item)
    <div class="col-4">
            <div class="card">
            <img src="{{asset('gambar/'. $item->poster)}}" alt="Card image cap">
            <div class="card-body">
                <h3>{{$item->judul}}</h3>
                <p class="card-text">{{$item->ringkasan}}</p>
            </div>
        </div>
    </div>    
        @endforeach

</div>


@endsection