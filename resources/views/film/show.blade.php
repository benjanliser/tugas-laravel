@extends('layout.master')

@section('judul')

Halaman Detail  Film {{$film->judul}}
    
@endsection


@section('content')

<img src="{{asset('gambar/' .$film->poster)}}" alt="">
<h1> {{$film->judul}} </h1>
<p> {{$film->ringkasan}} </p>


<h2> Komentar </h2>
@foreach ($film->kritik as $item) 
<div class="card">
    <div class="card-body">
      <h5><b> {{$item->user->name}} </b></h5>
      <p class="card-text"> {{$item->kritik}} </p>

    </div>
</div>
    
@endforeach

<form action="/kritik" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label > Komentar</label>
      <input type="hidden" name="film_id" value="{{$film->id}}" id="">
      <textarea name="isi" class="form-control" cols="30" rows="10"></textarea>
      
    </div>

    @error('isi')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror    
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
  <a href="/film" class="btn btn-secondary btn-info my-3"> Kembali </a>

@endsection