<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Profile;


class ProfileController extends Controller
{
    public function index(){
        $profil = Profile::where('user_id', Auth::id())->first();
        
        return view ('profile.index',compact('profil'));


    }

    public function update(Request $request, $id){

        $request->validate([
           
            'umur' => 'required',
            'bio' => 'required',
            'alamat' => 'required',
            ]);

            $profil = Profile::find($id);
            $profil->umur = $request ['umur'];
            $profil->bio = $request ['bio'];
            $profil->alamat = $request ['alamat'];
            $profil->save();

            return redirect('/profil');

    }
    
}
