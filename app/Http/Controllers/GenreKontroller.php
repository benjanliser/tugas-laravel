<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Genre;

class GenreKontroller extends Controller
{
    public function create(){
        return view('kategori.create');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
            'deskripsi' => 'required',
        ]);

        DB::table('genre')->insert([
            'nama' =>  $request['nama'],
            'deskripsi' => $request['deskripsi']
            
        ]);
        return redirect('/kategori');


    }
    public function index(){
        $genre = Genre::all();
        return view('kategori.index', compact('genre') );

       
    }
    public function show($id){
        $genre = Genre::find($id);
        return view('kategori.show', compact ('genre'));


    }

    public function edit($id){
        $genre = DB::table('genre')->where('id',$id)->first();
        return view('kategori.edit', compact ('genre'));

    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required',
            'deskripsi' => 'required',
        ]);
        $query = DB::table('genre')
              ->where('id', $id)
              ->update([
                  'nama' => $request['nama'],
                  'deskripsi' => $request['deskripsi'],
              ]);
              return redirect ('/kategori');

    }

    public function destroy($id){
        DB::table('genre')->where('id',$id)->delete();
        return redirect('kategori');

    }

}
