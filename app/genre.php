<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class genre extends Model
{
    protected $table = 'genre';
    protected $fillable = ['nama','deskripsi'];


    public function film()
{
    return $this->hasMany('App\Film');

}
}

