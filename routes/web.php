<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::get('/form ', 'AuthController@form');

Route::post('/welcome','AuthController@kirim');

Route::get('/data-table', function(){
    return view('table.data-table');

});

Route::get('table', function(){

    return view('table.table');
});


Route::group(['middleware' => ['auth']], function () {
    // CRUD Genre
Route::get('/kategori/create','GenreKontroller@create');
Route::post('/kategori', 'GenreKontroller@store');
Route::get('/kategori','GenreKontroller@index');
Route::get('/kategori/{kategori_id}','GenreKontroller@show');
Route::get('/kategori/{kategori_id}/edit','GenreKontroller@edit');
Route::put('/kategori/{kategori_id}','GenreKontroller@update');
Route::delete('/kategori/{kategori_id}','GenreKontroller@destroy');

//Update Profil
Route::resource('profil','ProfileController')->only(['index','update']);
Route::resource('kritik','KritikController')->only(['index','store']);



});

//CRUD Film
Route::resource('film','FilmController');
Auth::routes();


